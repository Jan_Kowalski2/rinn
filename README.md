# Rinn
Non-React template for website including SCSS and ES6 (babel) support.
## Getting started
First, you should have Node and NPM installed.
To start, run this command in terminal when in folder:
```
npm install
```
Okey, so you are now able to start your own project.
## Gulp Commands
After dependencies have been installed, use two commands:
```
gulp watch (or) npm run watch
```
for development, and
```
gulp build (or) npm run build
```
for building the final product.
## Folder structure
Last thing is to know what these scripts which you have just downloaded do.
Make next sites in src/ folder. **Do not touch** src/js and src/css folders. 
Use src/dev folder instead. In this folder when running gulp watch command JS and CSS files will be bundled and moved to the src/js and src/css folders. Your development site is using them. 
### build Folder
It's final product folder, it contains bundled and minified files.
### src Folder
It's your folder where you create site. If you would like to change CSS or JS, do it in src/dev folder.
### vendor Folder
Just place plugins here. After build this folder will be copied to build folder. 
## File structure
```
│   package.json
│   package-lock.json
│   README.md
│   .gitignore
└───build - (This folder is builded after using gulp build command)
│   │   index.html - Your website with paths to minified JS and CSS files.
│   └───js
│       │   build.js
│       │   build.js.map
│       │   build.min.js
│   └───css
│       │   style.css
│       │   style.min.css
│   └───vendor
│       │   Your plugins from src file.
└───src
│   │   index.html
│   └───dev
│         └───scss
│               │   sassFile.scss - Place SCSS files here.
│         └───js
│               │   app.js - Place your JS files here. Use ES6 to import plugins to main App file.
│   └───js
│       │   Single ES5 JS file with map file used by dev site. (after using gulp watch or gulp build)
│   └───css
│       │   Single CSS file used by dev site. (after using gulp watch or gulp build)
│   └───vendor
│       │   Insert plugins here.
```
## Authors
**Piotr Kwiatek** - [piotr.kwiatek@outlook.com](piotr.kwiatek@outlook.com)
## License
This project is licensed under the MIT License.