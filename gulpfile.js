// Dev devependencies
const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const sourcemaps = require("gulp-sourcemaps");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const browserify = require("browserify");
const babel = require("babelify");
const rename = require("gulp-rename");
const sequence = require("run-sequence");
const del = require("del");
const htmlReplace = require("gulp-html-replace");
const uglifycss = require("gulp-uglifycss");
const uglifyjs = require("gulp-uglify");
// End of dependencies

// Paths
const sourcePath = "./src";
const devPath = `${sourcePath}/dev`;
const buildPath = "./build";
const buildFolders = [
  `${sourcePath}/**/*`,
  `!${devPath}`,
  `!${devPath}/**/*`
];
// End of paths

// CSS
gulp.task("sass", () => {
  return gulp
    .src(`${devPath}/scss/style.scss`)
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest(`${sourcePath}/css`));
});

gulp.task("autoprefixer", ["sass"], () => {
  return gulp
    .src(`${sourcePath}/css/**/*`)
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest(`${sourcePath}/css`));
});

// End of CSS

// JS
gulp.task("babel", () => {
  const bundler = browserify(`${devPath}/js/app.js`, {
    debug: true
  }).transform(
    babel.configure({
      presets: ["es2015"]
    })
  );
  bundler
    .bundle()
    .on("error", err => {
      console.error(err);
      this.emit("end");
    })
    .pipe(source("build.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(`${sourcePath}/js`));
});
// End of JS

// Build tasks
gulp.task("clean", () => {
  return del.sync(buildPath);
});

gulp.task("copy", () => {
  return gulp.src(buildFolders).pipe(gulp.dest(buildPath));
});

gulp.task("uglifycss", () => {
  return gulp
    .src(`${buildPath}/css/style.css`)
    .pipe(
      uglifycss({
        maxLineLen: 120
      })
    )
    .pipe(
      rename({
        suffix: ".min"
      })
    )
    .pipe(gulp.dest(`${buildPath}/css`));
});

gulp.task("uglifyjs", () => {
  return gulp
    .src(`${buildPath}/js/**/*.js`)
    .pipe(uglifyjs())
    .pipe(
      rename({
        suffix: ".min"
      })
    )
    .pipe(gulp.dest(`${buildPath}/js`));
});

gulp.task("htmlReplace", () => {
  gulp
    .src(`${buildPath}/*.html`)
    .pipe(
      htmlReplace({
        css: "css/style.min.css",
        js: "js/build.min.js"
      })
    )
    .pipe(gulp.dest(buildPath));
});

// End of build tasks

// Main tasks
gulp.task("build", () => {
  sequence(
    "clean",
    "autoprefixer",
    "babel",
    "copy",
    "uglifycss",
    "uglifyjs",
    "htmlReplace"
  );
});

gulp.task("watch", ["autoprefixer", "babel"], () => {
  gulp.watch(`${devPath}/scss/**/*.scss`, ["autoprefixer"]);
  gulp.watch(`${devPath}/js/**/*.js`, ["babel"]);
});

// End of main tasks
