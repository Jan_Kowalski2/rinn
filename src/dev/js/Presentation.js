export default class Presentation {

    constructor({id, list} = {}) {
        this.requiredParameter(id, "string");
        this.requiredParameter(list, "string");

        this.container = document.querySelector(id);
        this.list = document.querySelector(list);
        
        this._availableSections = [];
        this._currentSection = 0;
        this._currentSubsection = 0;
        this._presentationActive = false;

        this._availableSections = this.setAvailableSections(this.list);
    }

    requiredParameter(param, type) {
        if (param === null || param === undefined) {
            throw Error(`Required parameter, ${param} is missing :(`);
        }
        else if (typeof(param) !== type) {
            throw Error(`${param} should be a ${type}.`);
        }
    }

    setAvailableSections() {
        let { children: sections } = this.list;

        for (let i = 0; i < sections.length; i++) {
            this._availableSections.push(sections[i].children[0].children.length);
        }

    }


}