(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Presentation = function () {
    function Presentation() {
        var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
            id = _ref.id,
            list = _ref.list;

        _classCallCheck(this, Presentation);

        this.requiredParameter(id, "string");
        this.requiredParameter(list, "string");

        this.container = document.querySelector(id);
        this.list = document.querySelector(list);

        this._availableSections = [];
        this._currentSection = 0;
        this._currentSubsection = 0;
        this._presentationActive = false;

        this._availableSections = this.setAvailableSections(this.list);
    }

    _createClass(Presentation, [{
        key: "requiredParameter",
        value: function requiredParameter(param, type) {
            if (param === null || param === undefined) {
                throw Error("Required parameter, " + param + " is missing :(");
            } else if ((typeof param === "undefined" ? "undefined" : _typeof(param)) !== type) {
                throw Error(param + " should be a " + type + ".");
            }
        }
    }, {
        key: "setAvailableSections",
        value: function setAvailableSections() {
            var sections = this.list.children;


            for (var i = 0; i < sections.length; i++) {
                this._availableSections.push(sections[i].children[0].children.length);
            }
        }
    }]);

    return Presentation;
}();

exports.default = Presentation;

},{}],2:[function(require,module,exports){
"use strict";

var _Presentation = require("./Presentation");

var _Presentation2 = _interopRequireDefault(_Presentation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {

    var presentation = new _Presentation2.default({ container: "#presentation", list: ".presentation__list" });
})();

},{"./Presentation":1}]},{},[2])

//# sourceMappingURL=build.js.map
